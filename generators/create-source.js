var _ = require('lodash');
var fs = require('fs');
var Promise = require('bluebird');

function run(pipeline, settings) {
    var list = pipeline.getKey('YAMLToJSONResult');
    return Promise.resolve(list)
        .then(list => {
            let sources = {};
            _.forEach(list, (item) => {
                // iterate over languages
                _.forEach(item.concept, (definition, langIdentifier) => {
                    // make sure new languages are added
                    if(!sources.hasOwnProperty(langIdentifier)) {
                        sources[langIdentifier] = [];
                    }
                    sources[langIdentifier].push(definition.source);
                });
            });
            return sources;
        })
        .then((sources) => {
            var items = {};
            _.forEach(sources, (data, langIdentifier) => {
                let tmp = _.union(data);
                tmp =  _.map(tmp, _.trim).filter(Boolean);
                items[langIdentifier] = tmp;
            });
            return items;
        })
        .then((sources) => {
            return Promise.resolve(settings.output(sources));
        });
}

function step(settings) {
    return {
        settings: settings || {},
        run: run
    }
}

module.exports.step = step;
module.exports.run = run;
