var _ = require('lodash');
var fs = require('fs');
var path = require('path');
var Promise = require('bluebird');
var yaml = require('js-yaml');

var glob = require('glob');

function run(pipeline, settings) {
    settings.filename = settings.filename || 'collection-decorated';

    return Promise.resolve()
        .then(findFiles(pipeline, settings))
        .then(readFiles(pipeline, settings))
        .spread(mergeTags(pipeline, settings))
        .spread(decorateConcepts(pipeline, settings));
}

function step(settings) {
    return {
        settings: settings || {},
        run: run
    }
}

function findFiles(pipeline, settings) {
    return () => {
        return new Promise((resolve, reject) => {
            glob(`${pipeline.getBasePath()}/raw_files/tags/**/*.yml`, null, function (err, files) {
                if(err) {
                    reject(err);
                }
                resolve(files);
              });
        });
    }
}

function readFiles(pipeline) {
    return (files) => {
        var definitions = [];
        var tags = [];
        _.forEach(files, (filePath) => {
            var fileInfo = path.parse(filePath);
            var fileObj = yaml.safeLoad(fs.readFileSync(filePath, 'utf8'));
            if(fileInfo.name == 'definition') {
                definitions.push(fileObj);
            } else {
                tags.push(fileObj);
            }
        });
        return [definitions, tags];
    }
}

function mergeTags(pipeline) {
    return (definitions, tags) => {
        pipeline.addLog(`Collected ${definitions.length} tag definitions with in total ${tags.length} tags`);
        // write tags to file
        pipeline.writeFile({
            path: `${pipeline.getOutputDirPath()}/tags.json`,
            data: JSON.stringify(definitions, null, 2),
            title: 'Taggar',
            description: 'En lista med alla taggar'
        });
        let hashTable = {};
        let finalTags = [];
        // hash em
        _.map(definitions, (definition) => {
            hashTable[definition.slug] = definition; 
        });
        _.map(tags, tag => {
            if(hashTable[tag.tag_slug]) {
                var tagObj = new Tag(hashTable[tag.tag_slug]);
                tagObj.addTagData(tag);
                // list
                finalTags.push(tagObj.output());
            } else {
                pipeline.addLog(`Tag: ${tag.tag_slug} is missing definition and is excluded`);
            }
        });
        return [hashTable, finalTags];
    }
}

function decorateConcepts(pipeline, settings) {
    return function(hashTable, tags) {
        // get concepts
        var concepts = pipeline.getKey('YAMLToJSONResult');
        return Promise.mapSeries(tags, (issue) => {
            /**
             * TODO: delete this ugly forEach()
             */
            _.forEach(concepts, (concept) => {
                if(concept.slug == issue.concept_slug) {
                    // decorate
                    concept.tags = concept.tags || [];
                    concept.tags.push(issue);
                } else {
                    // set default tag if concept doesnt have one
                    /*
                    concept.tags = [{
                        tag: 'ggstatus',
                        name: hashTable['ggstatus'].name,
                        review_steps: hashTable['ggstatus'].review_steps,
                    }]
                    */
                }
            });
        })
        .then(function() {
            // all issues ran
            pipeline.setKey('YAMLToJSONResult', concepts);
            pipeline.writeFile({
                path: `${pipeline.getOutputDirPath()}/${settings.filename}.json`,
                data: JSON.stringify(concepts, null, 2),
                title: 'Begreppslista',
                description: 'En lista i .json format innehållande samtliga begrepp'
            });
            return concepts;
        });
    }
}

/**
 * This helper class merges the tag definition with the tag
 */
class Tag {
    constructor(tagDefintion) {
        this.tagDefinition;
        this.reviewSteps = tagDefintion.review_steps;
        this.reviewStepsHash = {};
        this.stepProgressHash = {};
        this.tagData;
        this.tagDefinition = tagDefintion;
        this.reviewLength = this.reviewSteps.length;

        // store review steps for quick access
        _.forEach(this.tagDefinition.review_steps, (step) => {
            this.reviewStepsHash[step.name] = _.cloneDeep(step);
        });
    }
    addTagData(data) {
        this.tagData = data;
        _.forEach(this.tagData.review_steps, (step) => {
            this.stepProgressHash[step.name] = _.cloneDeep(step);
        });
    }
    output() {
        var outputObj = {
            tag: this.tagData.tag_slug,
            concept_slug: this.tagData.concept_slug,
            name: this.tagDefinition.name,
            review_steps: [],
            review_length: this.reviewLength
        };
        _.forEach(this.reviewSteps, (mainStep) => {
            let step = {};
            step.name = this.reviewStepsHash[mainStep.name].name;
            step.description = this.reviewStepsHash[mainStep.name].description;
            // check if step is in progress
            if(this.stepProgressHash[mainStep.name]) {
                // transfer comments
                if(this.stepProgressHash[mainStep.name].comments) {
                    step.comments = this.stepProgressHash[mainStep.name].comments;
                }
                // transfer status
                if(this.stepProgressHash[mainStep.name].status) {
                    step.status = this.stepProgressHash[mainStep.name].status;
                }
            }
            outputObj.review_steps.push(step);
        });
        // find first "non complete review"
        outputObj.currentStep = _.findLast(outputObj.review_steps, (step) => {
            return (step.status == 'ok');
        });
        var fullfilled = _.filter(outputObj.review_steps, (step) => {
            return (step.status == 'ok');
        });
        outputObj.fullfilled = fullfilled.length;
        // check percent complete
        outputObj.percentDone = (outputObj.fullfilled / outputObj.review_length) * 100
        return outputObj;
    }
}

module.exports.step = step;
module.exports.run = run;
