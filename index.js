var _ = require('lodash');

var infopack = require('infopack');
var markupGenerator = require('sbp-markup-generator-official');
var ymlToJson = require('sbp-utils-yml-to-json');

var jsonToXlsx = require('infopack-gen-json-to-xlsx');

var tagGenerator = require('./generators/tag-generator');
var createSource = require('./generators/create-source');

var pipeline = new infopack.Pipeline();

// generate json file from yamls
pipeline.addStep(ymlToJson.step({
	omitFile: true,
	readPath: './raw_files/concepts'
}));

pipeline.addStep(createSource.step({
	output: (data) => {
		pipeline.writeFile({
			path: `${pipeline.getOutputDirPath()}/sources.json`,
			data: JSON.stringify(data, '', 2),
			title: 'Lista på alla källor'
		});
	}
}));

pipeline.addStep(tagGenerator.step({
	filename: 'collection'
}));

// generate excel file from json
pipeline.addStep(jsonToXlsx.step({
	storageKey: 'YAMLToJSONResult',
	mapper: (conceptObj) => {
		return {
			"Term": _.get(conceptObj, 'concept.sv.term'),
			"Källa": _.get(conceptObj, 'concept.sv.source'),
			"Definition": _.get(conceptObj, 'concept.sv.definition'),
			"Kommentar": _.get(conceptObj, 'concept.sv.comment'),
			"Term_en": _.get(conceptObj, 'concept.en.term'),
			"Källa_en": _.get(conceptObj, 'concept.en.source'),
			"Definition_en": _.get(conceptObj, 'concept.en.definition'),
			"Kommentar_en": _.get(conceptObj, 'concept.en.comment')
		};
	}
}));

// run pipeline
pipeline.run()
	.then(function(data) {
		// generate static page
		pipeline.generateStaticPage();
		console.log(pipeline.prettyTable());
	});
