# SBP Begreppslista - Den öppna begreppslistan

Detta projekt är ett initiativ för att samla och harmonisera begrepp. Det finns inga begräsningsar för vilka begrepp som är lämpliga att lagra här men listan har ett naturligt fokus på samhällsbyggnad.

## Webbplats

http://nrb.sbplatform.se

## Förvaltare

Vi som förvaltar detta projekt är:

* [Johan Asplund](https://www.linkedin.com/in/bimjohan)
* [Rogier Jongeling](https://www.linkedin.com/in/rogierjongeling/)
* [Salih Sen](https://www.linkedin.com/in/salih-sen-b0232126)

Saknas ditt namn? Skapa ett ändringsförslag [här](https://gitlab.com/swe-nrb/sbp-begreppslista/edit/master/README.md)

## Bidra  

Vem som helst kan komma in och påverka innehållet i begreppslistan, Innehållet styrs huvudsakligen av två typer av filer, begreppsfiler och taggfiler.

### Begreppsfil  

Du kan lägga till dina egna begrepp genom att skapa en begreppsfil i mappen
`/raw_files/concepts`. För att föreslå begrepp måste du vara inloggad, om du inte har ett gitlab konto så kan du skapa det [här](https://gitlab.com/users/sign_in) (det är kostnadsfritt).

**Mall**  

Det finns en [mall](https://gitlab.com/swe-nrb/sbp-begreppslista/raw/master/support_files/schemas/concept_v1.yml) under mappen `/support_files/schemas`.

### Taggfil

Begrepp kan "dekoreras" med taggar för att påvisa vissa egenskaper på begreppen (t.ex. om begreppet är kvalitetsgranskat).
En tagg definieras genom att skapa en mapp med taggens namn och lägga in en `definition.yml`
med bl.a. information om godkännandeprocessen för att ett begrepp ska kunna få taggen.

Taggmappen läggs i mappen `/raw_files/tags`.

**Mall**

Det finns en [mall](https://gitlab.com/swe-nrb/sbp-begreppslista/raw/master/support_files/schemas/tag_v1.yml) under mappen `/support_files/schemas`.

## Namngivning av filer

Ange begreppet som filnamn med datorvänliga tecken:

* a-z
* 0-9
* - (dash) | _ (underscore) | , (comma)

__Exempel__

anlaggningsmodell-trafikverket.yml

## Steg-för-steg guider  

### Ansök om medlemskap (behövs för att lämna förbättringsförslag)  

1. Logga in på Gitlab
2. Ansök om medlemskap genom att klicka på request access under [projektets sida](https://gitlab.com/swe-nrb/sbp-begreppslista)
3. Direkt du har blivit godkänd kan du följa stegen under bidra

### Lägg till begrepp genom att skapa egna filer

1. Kopiera mallen [LÄNK](https://gitlab.com/swe-nrb/sbp-begreppslista/raw/master/support_files/schemas/concept_v1.yml)
2. Skapa en ny fil under mappen underlag [LÄNK](https://gitlab.com/swe-nrb/sbp-begreppslista/new/master/raw_files/concepts?file_name=filename.yml) (om du inte är inloggad eller har ansökt om medlemskap så kommer du till en felsida)
3. Benämna filen enligt ovan (__OBS!__ Ange benämningen utan åäö och med små bokstäver, avsluta med filändelsen `.yml`, t.ex. "anlaggningsmodell.yml". Filnamn ska vara exakt samma som fältet `filename` i mallfil)
4. Spara som en ny utvecklingsgren (skapa ny "branch")
5. Skapa en ny ändringsbegäran (skapa en "merge request" från din nya utvecklingsgren till "master")
6. Förslag bearbetas av begreppslistans förvaltningsgrupp
